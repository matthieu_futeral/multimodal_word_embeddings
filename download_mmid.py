# -*- coding: utf-8 -*-

"""
Created on 05/05/2021

@author: matthieufuteral-peter
"""


import os
import shutil
import glob
import random
import sys
import re
from logger import logger
import subprocess
from subprocess import PIPE
from multiprocessing import Pool, cpu_count

logger(f"CPU available {cpu_count()}")

def append_to_file(name, txt_list):
    with open(f'{name}.txt', 'a') as f:
        f.write('\n'.join(txt_list) + '\n')
    pass


logger.info(f"Start downloading {sys.argv[1]} MMID dataset")

# Set dirsave
os.chdir(sys.argv[2])

os.makedirs(f"data/mmid_{sys.argv[1]}", exist_ok=False)
os.chdir(f"./data/mmid_{sys.argv[1]}")


f_train = open('train.txt', 'w').close()
f_dev = open('dev.txt', 'w').close()
f_test = open('test.txt', 'w').close()


url = "https://multilingual-images.org/downloads.html"
args = ['wget %s' % url]
subprocess.run(args, shell=True, stdout=PIPE)


file = open("downloads.html", 'r')
content = file.read()
file.close()

reg1 = "((?<=<td>)english-[0-9]+(?=</td>))"
reg2 = "((?<=<td>)[a-zA-Z]*(?=</td>))"
reg3 = "((?<=<td>)[a-zA-Z]*-[a-zA-Z]*(?=</td>))"

out = re.findall(reg1, content)
out += re.findall(reg2, content)
out += re.findall(reg3, content)
out = set(out)

os.remove("downloads.html")

if sys.argv[1] == 'large':
    img_url = "https://s3.amazonaws.com/mmid-pds/language_image_packages/scale-{}-package.tgz"
elif sys.argv[1] == 'mini':
    img_url = "https://s3.amazonaws.com/mmid-pds/mini_language_image_packages/mini-{}-package.tgz"
else:
    raise AttributeError("Argument 'large' or 'mini' missing")


def main(args):

    out, idx, lang, img_url = args
    logger.info(f"Start downloading {lang}")
    train, dev, test = [], [], []

    lg_img_url = img_url.format(lang)

    # Create folders
    os.makedirs(lang, exist_ok=False)
    os.chdir(lang)

    # Download
    subprocess.run(['wget %s' % lg_img_url], shell=True, stdout=PIPE)

    # Uncompress
    fname = os.path.basename(lg_img_url)
    if os.path.exists(fname):
        subprocess.run(['tar -xvf %s' % fname], shell=True, stdout=PIPE)
        os.remove(fname)

        os.chdir('./..')

        # Build train, dev, test
        fname = fname[:-4]
        relative_path = f"./{lang}/{fname}/*"
        data = [txt for txt in glob.glob(relative_path) if bool(re.match(r"^[0-9]*$", os.path.basename(txt)))]

        # Check if there is at least one image and a word.txt in the folder
        cleaned_data = []
        for path in data:
            content = os.listdir(path)
            if ("01.jpg" in content) and ("word.txt" in content):
                cleaned_data.append(path)
        data = cleaned_data

        # Random sampling
        random.shuffle(data)
        l = len(data)
        train = data[:int(0.8*l)]
        dev = data[int(0.8*l): int(0.9*l)]
        test = data[int(0.9*l):]

        append_to_file('train', train)
        append_to_file('test', test)
        append_to_file('dev', dev)

        logger.info(f"{lang.upper()} done - Remaining languages : {len(out) - idx}")

    else:
        os.chdir('./..')
        shutil.rmtree(lang)
        logger.info(f"{lang.upper()} FAIL - Remaining languages : {len(out) - idx}")

    pass


# Multiprocessing
pool = Pool(cpu_count())
params = [(out, idx, lang, img_url) for idx, lang in enumerate(out, 1)]
pool.map(main, params)

