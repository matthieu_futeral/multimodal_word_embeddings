# -*- coding: utf-8 -*-

"""
Created on 10/05/2021

@author: matthieufuteral-peter
"""

import os
import re
import pdb
import json
import sys
sys.path.append("./util")  # Allow util import
sys.path.append(os.path.join(os.getcwd(), "models"))  # Allow models import
from logger import logger
import argparse
from tqdm import tqdm
import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from util import load_model, cosine_similarity
from torch.utils.data import DataLoader
from transformers import BertTokenizer, BertModel
from imagevec import ImageVec
from scipy.stats import pearsonr, spearmanr
import fasttext


class load_TestData:

    def __init__(self, args):

        # set args
        self.args = args

        # Load eval data
        folder_name = "subtask2-crosslingual" if args.crosslingual else "subtask1-monolingual"
        data_name, key_name = f"{args.lang}.test.data.txt", f"{args.lang}.test.gold.txt"
        path_data = os.path.join("data", "SemEval17-Task2", "test", folder_name, "data", data_name)
        path_key = os.path.join("data", "SemEval17-Task2", "test", folder_name, "keys", key_name)

        self.data = open(path_data, 'r').read().split('\n')[:-1]
        print(f"Length original test set : {len(self.data)} - LANG : {args.lang.upper()}")

        self.keys = [float(k) for k in open(path_key, 'r').read().split('\n')[:-1]]

        with open(os.path.join("results", args.exp_name, "vocab.json"), "r") as fj:
            self.vocabulary = json.load(fj)

        self.handle_oov()

        # Tokenizer
        if args.character_level:
            with open(os.path.join("results", args.exp_name, "char_vocab.json"), "r") as fj:
                self.character_vocab = json.load(fj)

        if not args.scratch_embeddings and not args.fasttext_embeddings:
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')

        if args.fasttext_embeddings:

            self.max_length_char = 128
            self.max_n_word_query = 20

            self.init_fasttext_processing()

        if args.word_embeddings:
            initial_len = len(self.data)
            self.data = [d for i, d in enumerate(self.data) if i in self.index]
            self.per_vocab = len(self.data) / initial_len
            print(f"Length after removing OOV test set : {len(self.data)} - LANG : {args.lang.upper()}")

            self.keys = [k for i, k in enumerate(self.keys) if i in self.index]

        else:
            self.per_vocab = np.sum(self.not_oov) / len(self.not_oov)

    def handle_oov(self):
        pos, index, not_oov = [], [], []
        for idx, word_pairs in enumerate(self.data):
            word1, word2 = word_pairs.split("\t")
            word1 = re.sub("\W+", ' ', word1.lower()).strip()
            word1 = [w.lower().strip() for w in word1.split()]

            word2 = re.sub("\W+", ' ', word2.lower()).strip()
            word2 = [w.lower().strip() for w in word2.split()]

            len_word1 = len([w for w in word1 if w in self.vocabulary.keys()])
            len_word2 = len([w for w in word2 if w in self.vocabulary.keys()])
            if (len_word1 == len(word1)) and (len_word2 == len(word2)):
                index.append(idx)
                not_oov.append(1)
            else:
                not_oov.append(0)
        self.index = index
        self.not_oov = not_oov

    def __getitem__(self, item):

        word1, word2 = self.data[item].split("\t")

        if not self.args.scratch_embeddings and not self.args.fasttext_embeddings:

            tokens1 = self.tokenizer(word1, add_special_tokens=False, padding='max_length', return_tensors='pt')['input_ids']
            tokens2 = self.tokenizer(word2, add_special_tokens=False, padding='max_length', return_tensors='pt')['input_ids']
            return tokens1.squeeze(), tokens2.squeeze()
        
        elif self.args.scratch_embeddings and not self.args.character_level:

            word1 = re.sub("\W+", ' ', word1.lower()).strip()
            word1 = [w.lower().strip() for w in word1.split() if w]

            word2 = re.sub("\W+", ' ', word2.lower()).strip()
            word2 = [w.lower().strip() for w in word2.split()]

            tokens1 = torch.LongTensor([self.vocabulary[w] for w in word1])
            tokens2 = torch.LongTensor([self.vocabulary[w] for w in word2])
            return tokens1, tokens2

        elif not self.args.scratch_embeddings and self.args.fasttext_embeddings:

            tokens1 = torch.LongTensor(self.format_subword_tokens(word1))
            tokens2 = torch.LongTensor(self.format_subword_tokens(word2))

            return tokens1, tokens2

        else:

            tokens1 = torch.LongTensor([self.character_vocab[c.lower()] for c in list(word1)])
            tokens2 = torch.LongTensor([self.character_vocab[c.lower()] for c in list(word2)])

            word1 = re.sub("\W+", ' ', word1.lower()).strip()
            word1 = [w.lower().strip() for w in word1.split() if w]

            word2 = re.sub("\W+", ' ', word2.lower()).strip()
            word2 = [w.lower().strip() for w in word2.split()]

            if self.not_oov[item]:
                w1 = torch.LongTensor([self.vocabulary[w] for w in word1])
                w2 = torch.LongTensor([self.vocabulary[w] for w in word2])
            else:
                w1 = torch.LongTensor([0])
                w2 = torch.LongTensor([0])

            return tokens1, w1, tokens2, w2

    def __len__(self):
        return len(self.data)

    def collate_fn(self, batch):
        if not self.args.character_level and not self.args.fasttext_embeddings:
            tok1, tok2 = list(zip(*batch))
            tok1 = pad_sequence(tok1, batch_first=True, padding_value=0)
            tok2 = pad_sequence(tok2, batch_first=True, padding_value=0)
            return tok1, tok2
        elif not self.args.character_level and self.args.fasttext_embeddings:
            tok1, tok2 = list(zip(*batch))
            return torch.stack(tok1), torch.stack(tok2)
        else:
            tokens1, w1, tokens2, w2 = list(zip(*batch))
            tok1 = pad_sequence(tokens1, batch_first=True, padding_value=0)
            tok2 = pad_sequence(tokens2, batch_first=True, padding_value=0)
            words1 = pad_sequence(w1, batch_first=True, padding_value=0)
            words2 = pad_sequence(w2, batch_first=True, padding_value=0)
            return (words1, tok1), (words2, tok2)

    def init_fasttext_processing(self):
        logger.info("Start init FastText Load vocab")
        path_fasttext = "./data/fasttext/wit_trained"
        path_model = os.path.join(path_fasttext, "model.bin")
        self.ft = fasttext.load_model(path_model)

        with open(os.path.join("results", args.exp_name, "ft_vocabulary_to_vocab_idx.json"), "r") as fj:
            self.ft_vocabulary_to_vocab_idx = json.load(fj)

        self.len_ft_vocab = len(self.ft_vocabulary_to_vocab_idx)
        self.index_vocab_init_model = list(self.ft_vocabulary_to_vocab_idx.keys())

        self.start_subword_idx = self.ft.get_output_matrix().shape[0]
        self.end_subword_idx = self.ft.get_input_matrix().shape[0]
        self.shift_subword_idx = self.start_subword_idx - self.len_ft_vocab

        self.index_vocab_init_model += [i for i in range(self.start_subword_idx, self.end_subword_idx)]
        self.index_vocab_init_model = [int(idx) for idx in self.index_vocab_init_model]
        logger.info("Finish init FastText Vocabulary")

    def format_subword_tokens(self, words):

        tokens = []
        words = words[:self.max_n_word_query]
        for w in words:
            final_tokens = []
            sub_tokens = (self.ft.get_subwords(w)[1] + 1).tolist()

            if 1 <= sub_tokens[0] <= self.start_subword_idx:
                final_tokens.append(self.ft_vocabulary_to_vocab_idx[sub_tokens[0]])
            else:
                final_tokens.append(self.convert_ft_subword_idx_to_vocab_idx(sub_tokens[0]))

            for sub_tok in sub_tokens[1:]:
                final_tokens.append(self.convert_ft_subword_idx_to_vocab_idx(sub_tok))

            if len(final_tokens) >= self.max_length_char:
                final_tokens = final_tokens[:self.max_length_char]
            else:
                final_tokens += [0] * (self.max_length_char - len(final_tokens))

            tokens.append(final_tokens)

        if len(words) < self.max_n_word_query:
            tokens += [[0] * self.max_length_char for _ in range(self.max_n_word_query - len(words))]

        return tokens

    def convert_ft_subword_idx_to_vocab_idx(self, subword_id):
        return subword_id - self.shift_subword_idx


def test_similarity(model, dataloader, device, args):

    model.eval()
    logger.info("Start evaluating...")
    corr = []

    for batch_idx, (tok1, tok2) in tqdm(enumerate(dataloader, 1)):

        if not args.character_level:
            tok1 = tok1.to(device)
            tok2 = tok2.to(device)
        else:
            tok1, char_tok1 = tok1
            tok2, char_tok2 = tok2
            tok1 = tok1.to(device)
            tok2 = tok2.to(device)
            char_tok1 = char_tok1.to(device)
            char_tok2 = char_tok2.to(device)

        with torch.no_grad():
            if not args.baseline and not args.character_level:
                embeds1 = model.query_embedding(tok1)
                embeds2 = model.query_embedding(tok2)
            elif not args.baseline and args.character_level:
                embeds1 = model.query_embedding(tok1, char_tok1)
                embeds2 = model.query_embedding(tok2, char_tok2)
            else:
                attn_mask1 = 1. * (tok1 != 0)
                attn_mask2 = 1. * (tok2 != 0)
                n_tokens1 = torch.sum(attn_mask1, dim=1)
                n_tokens2 = torch.sum(attn_mask2, dim=1)

                embeds1 = model(tok1)
                embeds2 = model(tok2)

                embeds1 = torch.sum(attn_mask1.unsqueeze(-1) * embeds1, dim=1) / n_tokens1.unsqueeze(1)
                embeds2 = torch.sum(attn_mask2.unsqueeze(-1) * embeds2, dim=1) / n_tokens2.unsqueeze(1)

        corr.append(cosine_similarity(embeds1, embeds2, batch=True, pairwise=True).cpu().diag().numpy())

    return np.concatenate(corr)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_name", type=str, required=True)
    parser.add_argument("--crosslingual", action="store_true", help="crosslingual or monolingual eval")
    parser.add_argument("--lang", type=str, required=True)
    parser.add_argument("--baseline", action="store_true", help="Compute score with baseline mBERT word embeddings")

    # Model info
    parser.add_argument("--pre_load_img", action="store_true")
    parser.add_argument("--scratch_embeddings", action="store_true")
    parser.add_argument("--character_level", action="store_true")
    parser.add_argument("--embedding_dim", type=int, default=300)
    parser.add_argument("--hidden_dim", type=int, default=768)
    parser.add_argument("--epoch", type=str, default="")
    parser.add_argument("--word_embeddings", action="store_true")
    parser.add_argument("--fasttext_embeddings", action="store_true")
    args = parser.parse_args()

    device = torch.device("cuda" if torch.cuda.is_available() else 'cpu')

    # Load eval data
    test_data = load_TestData(args)
    test_loader = DataLoader(test_data,
                             batch_size=64,
                             shuffle=False,
                             collate_fn=test_data.collate_fn,
                             num_workers=4)

    if args.baseline:
        bert = BertModel.from_pretrained('bert-base-multilingual-uncased')
        model = bert.embeddings.word_embeddings
        model.to(device)
    else:
        if args.epoch:
            path_model = os.path.join("results", args.exp_name, f"model_{args.epoch}.pt")
        else:
            path_model = os.path.join("results", args.exp_name, "model.pt")

        vocab_size = len(test_data.tokenizer) if not args.scratch_embeddings and not args.fasttext_embeddings\
            else len(test_data.vocabulary) + 1
        char_vocab_size = len(test_data.character_vocab) + 1 if args.character_level else None
        fasttext_model = None if not args.fasttext_embeddings else test_data.ft
        index_vocab = None if not args.fasttext_embeddings else test_data.index_vocab_init_model

        # if args.character_level:
        #    vocab_size = len(test_data.character_vocab) + 1
        
        model = ImageVec(vocab_size=vocab_size,
                         embedding_dim=args.embedding_dim,
                         hidden_dim=args.hidden_dim,
                         scratch_embeddings=args.scratch_embeddings,
                         character_level=args.character_level,
                         pre_load_img=args.pre_load_img,
                         char_vocab_size=char_vocab_size,
                         fasttext_embeddings=args.fasttext_embeddings,
                         fasttext_model=fasttext_model,
                         index_vocab=index_vocab)
        model = load_model(model=model, path_model=path_model, device=device)
        model.to(device)

    # Evaluate
    if args.baseline:
        print("BASELINE")
    logger.info(f"Start evaluating on SemEval 2017 Task 2 - Word similarity : {args.lang}")
    corr = test_similarity(model, test_loader, device, args).flatten()
    corr = np.maximum(corr, 0) * 4

    spearman_corr, p_value_spearman = spearmanr(a=corr, b=np.array(test_data.keys))
    pearson_corr, p_value_pearson = pearsonr(x=corr, y=np.array(test_data.keys))
    if not args.word_embeddings and not args.baseline:
        partial_spearman_corr, partial_p_value_spearman = spearmanr(a=np.array([c for i, c in enumerate(corr) if i in test_data.index]),
                                                                    b=np.array([k for i, k in enumerate(test_data.keys) if i in test_data.index]))
        partial_pearson_corr, partial_p_value_pearson = pearsonr(x=np.array([c for i, c in enumerate(corr) if i in test_data.index]),
                                                                 y=np.array([k for i, k in enumerate(test_data.keys) if
                                                                             i in test_data.index]))

    # Saving results
    logger.info("Evaluation done - Start saving results")
    task_type = "crosslingual" if args.crosslingual else "monolingual"
    if args.baseline:
        path_save = os.path.join("results", "baseline", "semeval_task2_wordsimilarity", task_type)
    else:
        if args.epoch:
            path_save = os.path.join("results", args.exp_name, "semeval_task2_wordsimilarity", f"epoch{args.epoch}", task_type)
        else:
            path_save = os.path.join("results", args.exp_name, "semeval_task2_wordsimilarity", task_type)
    os.makedirs(path_save, exist_ok=True)

    table_to_save = [w + '\t' + str(test_data.keys[i]) + '\t' + str(corr[i]) for i, w in enumerate(test_data.data)]
    table_to_save.insert(0, "word1\tword2\tgold\tpred")
    if not args.word_embeddings and not args.baseline:
        table_to_save = [w + '\t' + str(test_data.keys[i]) + '\t' + str(corr[i]) + '\t' + str(test_data.not_oov[i])
                         for i, w in enumerate(test_data.data)]
        table_to_save.insert(0, "word1\tword2\tgold\tpred\tin_vocab")

    with open(os.path.join(path_save, args.lang + '.txt'), 'w') as f:
        f.write('\n'.join(table_to_save))

    res = {"lang": args.lang,
           "spearman_corr": spearman_corr,
           "spearman_pvalue": p_value_spearman,
           "pearson_corr": pearson_corr,
           "pearson_pvalue": p_value_pearson,
           "coverage": test_data.per_vocab if not args.baseline else 1}
    if not args.word_embeddings and not args.baseline:
        res.update({"coverage_spearman_corr": partial_spearman_corr,
                    "coverage_spearman_pvalue": partial_p_value_spearman,
                    "coverage_pearson_corr": partial_pearson_corr,
                    "coverage_pearson_pvalue": partial_p_value_pearson
                    })
    with open(os.path.join(path_save, args.lang + '.json'), 'w') as fj:
        json.dump(res, fj)
