# -*- coding: utf-8 -*-

"""
Created on 31/05/2021

@author: matthieufuteral-peter
"""

import os
import re
import pdb
import json
import sys
sys.path.append("./util")  # Allow util import
sys.path.append(os.path.join(os.getcwd(), "models"))  # Allow models import
from logger import logger
import argparse
from tqdm import tqdm
import numpy as np
import torch
from util import load_model, cosine_similarity
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import pad_sequence
from transformers import BertTokenizer, BertModel
from imagevec import ImageVec
from scipy.stats import pearsonr, spearmanr


class load_TestData(object):

    def __init__(self):

        # Load eval data
        path_data = os.path.join("data", "VisSim", "similarity-ratings.tsv")
        self.data = open(path_data, 'r').read().split('\n')[1:-1]
        print(f"Length original test set : {len(self.data)}")

        # Tokenizer
        if not args.scratch_embeddings:
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')
        else:
            with open(os.path.join("results", args.exp_name, "vocab.json"), "r") as fj:
                self.vocabulary = json.load(fj)

        if args.character_level:
            with open(os.path.join("results", args.exp_name, "char_vocab.json"), "r") as fj:
                self.character_vocab = json.load(fj)

            self.handle_oov()
            print(f"Length after removing OOV test set : {len(self.data)}")

    def __getitem__(self, item):
        if not args.scratch_embeddings:
            word1, word2, sem_score, vis_score = self.data[item].split("\t")
            tokens1 = self.tokenizer(word1, add_special_tokens=False, padding='max_length', return_tensors='pt')['input_ids']
            tokens2 = self.tokenizer(word2, add_special_tokens=False, padding='max_length', return_tensors='pt')['input_ids']

            return tokens1.squeeze(), tokens2.squeeze(), float(vis_score)

        else:
            word1, word2, _, vis_score = self.data[item].split("\t")

            word1 = re.sub("\W+", ' ', word1.lower()).strip()
            word1 = [w.lower().strip() for w in word1.split() if w]

            word2 = re.sub("\W+", ' ', word2.lower()).strip()
            word2 = [w.lower().strip() for w in word2.split()]

            tokens1 = torch.LongTensor([self.vocabulary[w] for w in word1])
            tokens2 = torch.LongTensor([self.vocabulary[w] for w in word2])
            return tokens1, tokens2, float(vis_score)

    def __len__(self):
        return len(self.data)

    def handle_oov(self):
        pos, index = [], []
        for idx, word_pairs in enumerate(self.data):
            word1, word2, _, vis_score = word_pairs.split("\t")
            word1 = re.sub("\W+", ' ', word1.lower()).strip()
            word1 = [w.lower().strip() for w in word1.split()]

            word2 = re.sub("\W+", ' ', word2.lower()).strip()
            word2 = [w.lower().strip() for w in word2.split()]

            len_word1 = len([w for w in word1 if w in self.vocabulary.keys()])
            len_word2 = len([w for w in word2 if w in self.vocabulary.keys()])
            if (len_word1 == len(word1)) and (len_word2 == len(word2)):
                pos.append(word_pairs.lower())
                index.append(idx)
        self.data = pos
        self.index = index

    def collate_fn(self, batch):
        tok1, tok2, vis_score = list(zip(*batch))
        tok1 = pad_sequence(tok1, batch_first=True, padding_value=0)
        tok2 = pad_sequence(tok2, batch_first=True, padding_value=0)
        return tok1, tok2, vis_score


def test_similarity(model, dataloader, device, args):

    model.eval()
    logger.info("Start evaluating...")
    corr, vis_scores = [], []

    for batch_idx, (tok1, tok2, vis_score) in tqdm(enumerate(dataloader, 1)):

        tok1 = tok1.to(device)
        tok2 = tok2.to(device)
        with torch.no_grad():
            if not args.baseline:
                embeds1 = model.query_embedding(tok1)
                embeds2 = model.query_embedding(tok2)
            else:
                attn_mask1 = 1. * (tok1 != 0)
                attn_mask2 = 1. * (tok2 != 0)
                n_tokens1 = torch.sum(attn_mask1, dim=1)
                n_tokens2 = torch.sum(attn_mask2, dim=1)

                embeds1 = model(tok1)
                embeds2 = model(tok2)

                embeds1 = torch.sum(attn_mask1.unsqueeze(-1) * embeds1, dim=1) / n_tokens1.unsqueeze(1)
                embeds2 = torch.sum(attn_mask2.unsqueeze(-1) * embeds2, dim=1) / n_tokens2.unsqueeze(1)

        corr.append(cosine_similarity(embeds1, embeds2, batch=True, pairwise=True).cpu().diag().numpy())
        vis_scores.append(vis_score)

    return np.concatenate(corr), np.concatenate(vis_scores)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_name", type=str, required=True)
    parser.add_argument("--baseline", action="store_true", help="Compute score with baseline mBERT word embeddings")

    # Model info
    parser.add_argument("--pre_load_img", action="store_true")
    parser.add_argument("--scratch_embeddings", action="store_true")
    parser.add_argument("--embedding_dim", type=int, default=768)
    parser.add_argument("--character_level", action="store_true")
    parser.add_argument("--hidden_dim", type=int, default=768)
    parser.add_argument("--epoch", type=str, default="")
    args = parser.parse_args()

    device = torch.device("cuda" if torch.cuda.is_available() else 'cpu')


    # Load eval data
    test_data = load_TestData()
    test_loader = DataLoader(test_data,
                             batch_size=128,
                             shuffle=False,
                             collate_fn=test_data.collate_fn,
                             num_workers=2)

    # Load model

    if args.baseline:
        bert = BertModel.from_pretrained('bert-base-multilingual-uncased')
        model = bert.embeddings.word_embeddings
        model.to(device)
    else:
        if args.epoch:
            path_model = os.path.join("results", args.exp_name, f"model_{args.epoch}.pt")
        else:
            path_model = os.path.join("results", args.exp_name, "model.pt")

        vocab_size = len(test_data.tokenizer) if not args.scratch_embeddings else len(test_data.vocabulary) + 1

        if args.character_level:
            vocab_size = len(test_data.character_vocab) + 1

        model = ImageVec(vocab_size=vocab_size,
                         embedding_dim=args.embedding_dim,
                         scratch_embeddings=args.scratch_embeddings,
                         character_level=args.character_level,
                         hidden_dim=args.hidden_dim,
                         pre_load_img=args.pre_load_img)
        model = load_model(model=model, path_model=path_model, device=device)
        model.to(device)

    # Evaluate
    if args.baseline:
        print("BASELINE")
    logger.info("Start evaluating on Visual Word similarity")
    corr, vis_scores = test_similarity(model, test_loader, device, args)
    corr = corr.flatten()
    vis_scores = vis_scores.flatten()
    corr = np.maximum(corr, 0) * 4

    spearman_corr, p_value_spearman = spearmanr(a=corr, b=vis_scores)
    pearson_corr, p_value_pearson = pearsonr(x=corr, y=vis_scores)

    # Saving results
    logger.info("Evaluation done - Start saving results")
    if args.baseline:
        path_save = os.path.join("results", "baseline", "visual_wordsimilarity")
    else:
        if args.epoch:
            path_save = os.path.join("results", args.exp_name, "visual_wordsimilarity", f"epoch{args.epoch}")
        else:
            path_save = os.path.join("results", args.exp_name, "visual_wordsimilarity")
    os.makedirs(path_save, exist_ok=True)

    table_to_save = ["word1\tword2\tvis_gold\tvis_pred"]
    for i, w in enumerate(test_data.data):
        word1, word2, sem_score, vis_score = w.split("\t")
        table_to_save.append(word1 + '\t' + word2 + '\t' + str(vis_scores[i]) + '\t' + str(corr[i]))

    with open(os.path.join(path_save, 'en_visual_similarity.txt'), 'w') as f:
        f.write('\n'.join(table_to_save))

    res = {"name": "English visual similarity",
           "spearman_corr": spearman_corr,
           "spearman_pvalue": p_value_spearman,
           "pearson_corr": pearson_corr,
           "pearson_pvalue": p_value_pearson}
    with open(os.path.join(path_save, 'en_visual_similarity.json'), 'w') as fj:
        json.dump(res, fj)




