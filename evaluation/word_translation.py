# -*- coding: utf-8 -*-

"""
Created on 18/06/2021

@author: matthieufuteral-peter
"""


import os
import random
import json
import argparse
import re
import pdb
import json
import sys
sys.path.append("./util")  # Allow util import
sys.path.append(os.path.join(os.getcwd(), "models"))  # Allow models import
from logger import logger
import argparse
from tqdm import tqdm
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from imagevec import ImageVec
from util import cosine_similarity, load_model
from transformers import BertTokenizer



class build_source_sample(Dataset):
    def __init__(self, dic, vocab, args):
        super(build_source_sample, self).__init__()

        self.args = args
        self.dic = dic
        self.language_mapping = {"en": "english",
                                 "es": "spanish",
                                 "de": "german",
                                 "zh": "chinese",
                                 "it": "italian",
                                 "fr": "french",
                                 "pt": "portuguese"}

        self.abbrev_mapping = {v: k for k, v in self.language_mapping.items()}

        self.vocab = vocab
        self.build_vocab()

        lang1, lang2 = args.bilingual_dict.split("-")
        self.coverage, self.coverage_src, self.coverage_tgt = self.build_coverage(self.dic,
                                                                                  self.vocabulary,
                                                                                  self.language_mapping[lang1],
                                                                                  self.language_mapping[lang2])

        logger.info("Bilingual dictionary : {} - Coverage : {:.2f}".format(args.bilingual_dict.upper(),
                                                                           np.sum(self.coverage) * 100 / len(self.coverage)))

        if args.scratch_embeddings:
            self.coverage_dic = [wp for idx, wp in enumerate(self.dic) if self.coverage[idx]]
        else:
            self.coverage_dic = self.dic
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')

        self.query_words = list(set([wp[0] for wp in self.coverage_dic]))
        self.candidate_words = list(set([wp[1] for wp in self.coverage_dic]))

        self.build_ground_truth()

    def __len__(self):
        return len(self.query_words)

    def __getitem__(self, item):

        w1 = self.query_words[item]

        if self.args.scratch_embeddings:
            w1 = re.sub("\W+", ' ', w1.lower()).strip()
            tokens = torch.LongTensor([self.vocab[w1]])
        else:
            tokens = self.tokenizer(w1, add_special_tokens=False, padding='max_length', return_tensors='pt')['input_ids']
            tokens = tokens.squeeze()

        return tokens, w1

    def collate_fn(self, batch):
        tokens, w1 = list(zip(*batch))
        if self.args.scratch_embeddings:
            texts = pad_sequence(tokens, batch_first=True, padding_value=0)
        else:
            texts = torch.stack(tokens)
        return texts, list(w1)

    def build_vocab(self):
        with open(os.path.join("data", "mmid_large") + '/dict.json', 'r') as fj:
            self.id2word = json.load(fj)

        lang_keys = [self.language_mapping[l] for l in args.bilingual_dict.split("-")]
        if "english" in lang_keys:
            lang_keys += [f"english-{str(i).zfill(2)}" for i in range(1, 28)]
            english_words = []

        self.id2word = {k: v for k, v in self.id2word.items() if k in lang_keys}
        self.word2id = {lang: {v: k for k, v in self.id2word[lang].items()} for lang in self.id2word.keys()}

        self.vocabulary = {}
        self.len_vocab = 1
        for lang in self.word2id.keys():
            queries = list(self.word2id[lang].keys())

            words = []
            for query in queries:
                clean_query = re.sub("\W+", ' ', query.lower()).strip()
                words += [w.lower().strip() for w in clean_query.split()]

            if "english" in lang:
                english_words += [w for w in list(set(words)) if w]
            else:
                words = [w for w in list(set(words)) if w]

            if not "english" in lang:
                self.vocabulary[lang] = {w: idx for idx, w in enumerate(words, 1)}
                self.len_vocab += len(self.vocabulary[lang])

        if "english" in lang_keys:
            self.vocabulary["english"] = {w: idx for idx, w in enumerate(english_words, 1)}
            self.len_vocab += len(self.vocabulary["english"])

    def build_ground_truth(self):

        self.ground_truth = {}
        for word_pair in self.coverage_dic:
            if word_pair[0] not in self.ground_truth.keys():
                self.ground_truth[word_pair[0]] = [word_pair[1]]
            else:
                self.ground_truth[word_pair[0]].append(word_pair[1])

    @staticmethod
    def build_coverage(dic: list, vocabulary: dict, lang1: str, lang2: str):
        coverage = []
        coverage_w1, coverage_w2 = [], []
        for w1, w2 in dic:
            w1 = re.sub("\W+", ' ', w1.lower()).strip()
            w2 = re.sub("\W+", ' ', w2.lower()).strip()

            w1_covered = w1 in vocabulary[lang1].keys()
            w2_covered = w2 in vocabulary[lang2].keys()
            coverage.append(1 if w1_covered and w2_covered else 0)

            coverage_w1.append(1 if w1_covered else 0)
            coverage_w2.append(1 if w2_covered else 0)
        return coverage, coverage_w1, coverage_w2



class build_target_sample(Dataset):
    def __init__(self, vocab, vocabulary, target_lang, candidate_words, args):
        super(build_target_sample, self).__init__()

        self.args = args
        self.language_mapping = {"en": "english",
                                 "es": "spanish",
                                 "de": "german",
                                 "zh": "chinese",
                                 "it": "italian",
                                 "fr": "french",
                                 "pt": "portuguese"}

        self.vocab = vocab
        self.vocabulary = vocabulary[self.language_mapping[target_lang]]
        self.target_words = list(set(self.vocabulary.keys()))
        if not args.scratch_embeddings:
            self.target_words += candidate_words
            self.target_words = list(set(self.target_words))

            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')

    def collate_fn(self, batch):
        tokens, w1 = list(zip(*batch))
        if self.args.scratch_embeddings:
            texts = pad_sequence(tokens, batch_first=True, padding_value=0)
        else:
            texts = torch.stack(tokens)
        return texts, list(w1)

    def __len__(self):
        return len(self.target_words)

    def __getitem__(self, item):

        target_word = self.target_words[item]

        if self.args.scratch_embeddings:
            target_word = re.sub("\W+", ' ', target_word.lower()).strip()
            tokens = torch.LongTensor([self.vocab[target_word]])
        else:
            tokens = self.tokenizer(target_word, add_special_tokens=False, padding='max_length', return_tensors='pt')[
                'input_ids']
            tokens = tokens.squeeze()

        return tokens, target_word



def compute_embeddings(model, dataloader, device):

    model.eval()
    embeddings, t_words = [], []

    for batch_idx, (tokens, target_words) in tqdm(enumerate(dataloader)):

        tokens = tokens.to(device)

        with torch.no_grad():

            embeddings.append(model.query_embedding(tokens))
            t_words += target_words

    return torch.cat(embeddings, dim=0), t_words


def compute_recall_score(src_tgt_sorted, ground_truth, src_word2id, tgt_words):

    num_samples = []
    recall1, recall5, recall10 = [], [], []
    summary = ["src_word\ttgt_translation_words\tcandidates"]

    for src_w, tgt_ws in ground_truth.items():

        line = src_w + '\t . \t' + ' '.join(tgt_ws) + '\t . \t'
        num_samples.append(len(tgt_ws))
        id_src_w = src_word2id[src_w]
        candidates = [tgt_words[idx] for idx in src_tgt_sorted[id_src_w]]
        line += ' '.join(candidates)
        summary.append(line)

        # Recall@1
        if candidates[0] in tgt_ws:
            recall1.append(1)
        else:
            recall1.append(0)

        # Recall@5
        recall_5 = 0
        for tgt_candidate in candidates[:5]:
            if tgt_candidate in tgt_ws:
                recall_5 += 1
        recall5.append(recall_5)

        # Recall@10
        recall_10 = 0
        for tgt_candidate in candidates:
            if tgt_candidate in tgt_ws:
                recall_10 += 1
        recall10.append(recall_10)

    recall1 = [r / num_samples[idx] for idx, r in enumerate(recall1)]
    recall5 = [r / num_samples[idx] for idx, r in enumerate(recall5)]
    recall10 = [r / num_samples[idx] for idx, r in enumerate(recall10)]

    recall_a_1 = np.mean(recall1)
    recall_a_5 = np.mean(recall5)
    recall_a_10 = np.mean(recall10)

    return recall_a_1, recall_a_5, recall_a_10, summary



def get_word_translation_accuracy(src_emb, tgt_emb, source_sample, src_word2id, tgt_word2id):
    """
    Given source and target word embeddings, and a dictionary,
    evaluate the translation accuracy using the precision@k.
    """

    # Build dico
    dico = torch.LongTensor(len(source_sample.coverage_dic), 2)
    for idx, word_pair in enumerate(source_sample.coverage_dic):
        src_word, tgt_word = word_pair
        dico[idx, 0] = src_word2id[src_word]
        dico[idx, 1] = tgt_word2id[tgt_word]

    query = src_emb[dico[:, 0]]
    scores = query.mm(tgt_emb.transpose(0, 1))
    results = []
    top_matches = scores.topk(10, 1, True)[1]
    for k in [1, 5, 10]:
        top_k_matches = top_matches[:, :k].cpu()

        _matching = (top_k_matches == dico[:, 1][:, None].expand_as(top_k_matches)).sum(1).cpu().numpy()
        # allow for multiple possible translations
        matching = {}
        for i, src_id in enumerate(dico[:, 0].cpu().numpy()):
            matching[src_id] = min(matching.get(src_id, 0) + _matching[i], 1)
        # evaluate precision@k
        precision_at_k = 100 * np.mean(list(matching.values()))
        logger.info("%i source words - %s - Precision at k = %i: %f" %
                    (len(matching), "KNN", k, precision_at_k))
        results.append(('precision_at_%i' % k, precision_at_k))

    return results


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--bilingual_dict", type=str, required=True, help="example : en-es")
    parser.add_argument("--exp_name", type=str, required=True,
                        help="name of the experiment to find vocab and trained model")
    parser.add_argument("--visual", action="store_true", help="Use the visual dataset en-fr Sigurdsson et al.")
    parser.add_argument("--seed", type=int, default=42)

    # Model info
    parser.add_argument("--pre_load_img", action="store_true")
    parser.add_argument("--scratch_embeddings", action="store_true")
    parser.add_argument("--character_level", action="store_true")
    parser.add_argument("--embedding_dim", type=int, default=300)
    parser.add_argument("--hidden_dim", type=int, default=512)
    parser.add_argument("--epoch", type=str, default="")
    parser.add_argument("--word_embeddings", action="store_true")
    args = parser.parse_args()

    # Set seeds
    torch.cuda.manual_seed_all(args.seed)
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)
    random.seed(args.seed)

    # Set paths
    PATH = os.getcwd()
    PATH_EXP = os.path.join(PATH, "results", args.exp_name)
    PATH_MODEL = os.path.join(PATH_EXP, "model.pt" if not args.epoch else f"model_{args.epoch}.pt")
    PATH_VOCAB = os.path.join(PATH_EXP, "vocab.json")

    PATH_DATA = os.path.join(PATH, "data", "muse_data", "crosslingual", "dictionaries",
                             f"{args.bilingual_dict}.5000-6500.txt")
    if args.visual:
        PATH_DATA = os.path.join(PATH, "data", "muse_data", "crosslingual", "dictionaries",
                                 f"{args.bilingual_dict}.visual.5000-6500.txt")
    if not os.path.exists(PATH_DATA):
        raise NameError("Bilingual Dictionary does not exist")

    # Load vocab
    with open(PATH_VOCAB, "r") as fj:
        vocab = json.load(fj)

    # Load model
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    vocab_size = len(vocab) + 1
    model = ImageVec(vocab_size=vocab_size,
                     embedding_dim=args.embedding_dim,
                     hidden_dim=args.hidden_dim,
                     scratch_embeddings=args.scratch_embeddings,
                     character_level=args.character_level,
                     pre_load_img=args.pre_load_img)
    model = load_model(model, PATH_MODEL, device)
    model.to(device)

    # Load dict
    with open(PATH_DATA, "r") as f:
        bilingual_dic = f.read().split("\n")[:-1]

    bilingual_dic = [(word_pair.split()[0], word_pair.split()[1]) for word_pair in bilingual_dic]

    source_sample = build_source_sample(bilingual_dic, vocab, args)
    target_sample = build_target_sample(source_sample.vocab, source_sample.vocabulary,
                                        args.bilingual_dict.split("-")[-1], source_sample.candidate_words, args)
    ground_truth = source_sample.ground_truth
    percentage_coverage = np.sum(source_sample.coverage) / len(source_sample.coverage)

    src_dataloader = DataLoader(source_sample,
                                batch_size=128,
                                shuffle=False,
                                collate_fn=source_sample.collate_fn,
                                num_workers=2)

    tgt_dataloader = DataLoader(target_sample,
                                batch_size=128,
                                shuffle=False,
                                collate_fn=target_sample.collate_fn,
                                num_workers=2)


    src_emb, src_words = compute_embeddings(model, src_dataloader, device)
    tgt_emb, tgt_words = compute_embeddings(model, tgt_dataloader, device)
    tgt_word2id = {w: idx for idx, w in enumerate(tgt_words)}
    src_word2id = {w: idx for idx, w in enumerate(src_words)}

    src_tgt_similarity = cosine_similarity(src_emb, tgt_emb, pairwise=True)
    src_tgt_sorted = src_tgt_similarity.topk(10, 1, True)[1]
    # torch.argsort(src_tgt_similarity, descending=True, dim=1)[:, :10]

    recall_a_1, recall_a_5, recall_a_10, summary = compute_recall_score(src_tgt_sorted,
                                                                        ground_truth,
                                                                        src_word2id,
                                                                        tgt_words)

    logger.info("{} - Recall@1 : {:.2f}".format(args.bilingual_dict.upper(), recall_a_1 * 100))
    logger.info("{} - Recall@5 : {:.2f}".format(args.bilingual_dict.upper(), recall_a_5 * 100))
    logger.info("{} - Recall@10 : {:.2f}".format(args.bilingual_dict.upper(), recall_a_10 * 100))

    precision_results = get_word_translation_accuracy(src_emb, tgt_emb, source_sample, src_word2id, tgt_word2id)

    # Save results
    os.makedirs(os.path.join("results", args.exp_name, "bilingual_dict_muse"), exist_ok=True)
    results = {"src_lang": args.bilingual_dict.split("-")[0],
               "tgt_lang": args.bilingual_dict.split("-")[1],
               "Recall@1": recall_a_1 * 100,
               "Recall@5": recall_a_5 * 100,
               "Recall@10": recall_a_10 * 100,
               "Precision@1": precision_results[0][1],
               "Precision@5": precision_results[1][1],
               "Precision@10": precision_results[2][1],
               "coverage": percentage_coverage,
               "src_word_coverage": f"{np.sum(source_sample.coverage_src)} / {len(source_sample.coverage_src)}",
               "tgt_word_coverage": f"{np.sum(source_sample.coverage_tgt)} / {len(source_sample.coverage_tgt)}"}

    with open(os.path.join("results", args.exp_name, "bilingual_dict_muse",
                           args.bilingual_dict + ".json" if not args.visual else args.bilingual_dict + "_visual.json"), "w") as fj:
        json.dump(results, fj)

    with open(os.path.join("results", args.exp_name, "bilingual_dict_muse",
                           args.bilingual_dict + ".txt" if not args.visual else args.bilingual_dict + "_visual.txt"), "w") as f:
        f.write('\n'.join(summary))
