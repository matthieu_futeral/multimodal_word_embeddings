# -*- coding: utf-8 -*-

"""
Created on 05/05/2021

@author: matthieufuteral-peter
"""


from .imagevec import ImageVec


__all__ = ["ImageVec"]
