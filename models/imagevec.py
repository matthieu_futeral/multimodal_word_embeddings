# -*- coding: utf-8 -*-

"""
Created on 05/05/2021

@author: matthieufuteral-peter
"""

import os
import fasttext
import json
import pdb
import numpy as np
import torch
import torch.nn as nn
from util import cosine_similarity
from transformers import BertModel
from torchvision.models import resnet50
from logger import logger


"""
def parse_config(path_config: str):
    with open(path_config, "r") as fj:
        config = json.load(fj)

    # NLP
    
    config["language"] = 
"""



class ImageVec(nn.Module):
    """
    Implementation of the original Text-Image multimodal multilingual word embeddings extractor from Singhal et al. (2019)
    __call__ return a tuple (cosine similarity value, query representation, image representation)
    """
    def __init__(self, vocab_size: int, embedding_dim: int, hidden_dim: int, scratch_embeddings: bool, pre_load_img: bool,
                 temperature=1., character_level=False, fasttext_embeddings=False, fasttext_model=None, index_vocab=None,
                 char_vocab_size=None):
        super(ImageVec, self).__init__()

        self.pre_load_img = pre_load_img
        if self.pre_load_img:
            self.resnet = resnet50(pretrained=True)
            self.init_image_features()

        self.embedding_dim = embedding_dim
        self.scratch_embeddings = scratch_embeddings
        self.fasttext_embeddings = fasttext_embeddings
        self.character_level = character_level
        self.temperature = temperature

        if self.scratch_embeddings and not self.character_level:
            self.embedding = nn.Embedding(num_embeddings=vocab_size,
                                          embedding_dim=embedding_dim,
                                          padding_idx=0)
            self.batch_norm = nn.BatchNorm1d(num_features=embedding_dim)
        elif self.scratch_embeddings and self.character_level:

            self.embedding = nn.Embedding(num_embeddings=vocab_size,
                                          embedding_dim=embedding_dim,
                                          padding_idx=0)
            self.char_embedding = nn.Embedding(num_embeddings=char_vocab_size,
                                               embedding_dim=embedding_dim,
                                               padding_idx=0)
            self.cnn_block1 = nn.Sequential(nn.Conv1d(in_channels=embedding_dim, out_channels=embedding_dim, kernel_size=3), nn.ReLU(), nn.MaxPool1d(3))
            self.cnn_block2 = nn.Sequential(nn.Conv1d(in_channels=embedding_dim, out_channels=embedding_dim, kernel_size=4), nn.ReLU(), nn.MaxPool1d(4))
            self.cnn_block3 = nn.Sequential(nn.Conv1d(in_channels=embedding_dim, out_channels=embedding_dim, kernel_size=5), nn.ReLU(), nn.MaxPool1d(5))
            # self.max_pool = nn.MaxPool1d(kernel_size=3)
            # self.rnn_block = nn.LSTM(hidden_dim, embedding_dim, batch_first=True, bidirectional=True, dropout=0.2,
            #                          num_layers=2)
            # hidden_dim // 2
            self.batch_norm_cnn = nn.BatchNorm1d(num_features=embedding_dim)
            self.project = nn.Linear(embedding_dim * 3, embedding_dim)
            self.batch_norm = nn.BatchNorm1d(num_features=embedding_dim)
            self.linear = nn.Linear(embedding_dim * 2, embedding_dim)
            self.dropout = nn.Dropout(0.2)

        elif not scratch_embeddings and not self.fasttext_embeddings:
            bert = BertModel.from_pretrained('bert-base-multilingual-uncased')
            self.embedding = bert.embeddings.word_embeddings

            self.rnn_block = nn.LSTM(768, embedding_dim // 2, batch_first=True, bidirectional=True, dropout=0.2)
            # self.embedding_dim = 768
            # self.text_linear = nn.Linear(768, embedding_dim)
            # self.nlp = nn.ModuleList([self.embedding, self.text_linear])
            self.batch_norm = nn.BatchNorm1d(num_features=embedding_dim)
            self.text_batch_norm = nn.BatchNorm1d(num_features=embedding_dim)

        else:

            self.init_fasttext_embedding_layer(fasttext_model, index_vocab)
            self.rnn_block = nn.LSTM(embedding_dim, embedding_dim // 2, batch_first=True, bidirectional=True, dropout=0.2)
            self.batch_norm = nn.BatchNorm1d(num_features=embedding_dim)
            self.text_batch_norm = nn.BatchNorm1d(num_features=embedding_dim)

        self.relu = nn.LeakyReLU()
        self.init_layers(embedding_dim)
        

    def query_embedding(self, text, char_text=None):
        """
        Get query represensation of the text by averaging multilingual word embeddings
        :param text: Tensor of size (batch, max_length), text must be token ids
        :return: query representation of size (batch, embedding_dim)
        """

        if self.scratch_embeddings:

            attn_mask = 1. * (text != 0)
            embeds = self.embedding(text)

            n_tokens = torch.clamp(torch.sum(attn_mask, dim=1), min=1.)
            out = torch.sum(attn_mask.unsqueeze(-1) * embeds, dim=1)
            out = out / n_tokens.unsqueeze(1)

            if self.character_level:

                char_attn_mask = 1. * (char_text != 0)
                char_embeds = self.char_embedding(char_text)

                # lengths = attn_mask.sum(dim=-1).cpu()
                # packed = nn.utils.rnn.pack_padded_sequence(
                #    embeds, lengths, batch_first=True, enforce_sorted=False)
                char_embeds = char_embeds * char_attn_mask.unsqueeze(-1)

                h1 = self.cnn_block1(char_embeds.permute(0, 2, 1))
                h2 = self.cnn_block2(char_embeds.permute(0, 2, 1))
                h3 = self.cnn_block3(char_embeds.permute(0, 2, 1))
                out_cnn1 = self.batch_norm_cnn(h1.max(dim=2)[0])
                out_cnn2 = self.batch_norm_cnn(h2.max(dim=2)[0])
                out_cnn3 = self.batch_norm_cnn(h3.max(dim=2)[0])

                out_cnn = torch.cat((out_cnn1, out_cnn2, out_cnn3), dim=-1)
                out_cnn = self.project(out_cnn)

                cat_out = torch.cat((out, out_cnn), dim=-1)
                out = self.linear(cat_out)

                # _, (h_n, c_n) = self.rnn_block(packed)  # out_cnn.permute(0, 2, 1)

                # h_n[-1]

            return out

        elif self.fasttext_embeddings and not self.scratch_embeddings:

            batch_size, n_words, n_characters = text.size()
            attn_mask = 1. * (text[:, :, 0] != 0)
            subword_attn_mask = 1. * (text != 0)
            embeds = self.embedding(text).view(-1, n_characters, self.embedding_dim)

            lengths = torch.clamp(subword_attn_mask.sum(dim=-1).cpu().view(-1).long(), min=1)
            packed = nn.utils.rnn.pack_padded_sequence(embeds, lengths, batch_first=True, enforce_sorted=False)
            output, _ = self.rnn_block(packed)  # (h_n, c_n)

            out_unpacked, _ = nn.utils.rnn.pad_packed_sequence(output, batch_first=True)  # Unpack
            word_embeddings = out_unpacked.max(dim=1)[0].view(batch_size, n_words, -1)

            # embeds = self.embedding(text).sum(dim=-2) * attn_mask.unsqueeze(-1)

            n_tokens = torch.sum(attn_mask, dim=1)
            query_embeds = torch.sum(word_embeddings, dim=1)
            return query_embeds / n_tokens.unsqueeze(1)

        else:

            attn_mask = 1. * (text != 0)
            embeds = self.embedding(text)

            lengths = attn_mask.sum(dim=-1).cpu()
            packed = nn.utils.rnn.pack_padded_sequence(embeds, lengths, batch_first=True, enforce_sorted=False)

            output, _ = self.rnn_block(packed)
            out_unpacked, _ = nn.utils.rnn.pad_packed_sequence(output, batch_first=True)  # Unpack
            word_embeddings = out_unpacked.max(dim=1)[0]

            #n_tokens = torch.sum(attn_mask, dim=1)
            #query_embeds = torch.sum(word_embeddings, dim=1)
            return self.text_batch_norm(word_embeddings)  # query_embeds / n_tokens.unsqueeze(1)

    def image_embedding(self, img):
        """
        Compute image representation according to Singhal et al. (2019)
        :param img: raw image preprocess of shape (batch, n_channels, width, height)
        :return: img representation (batch, embedding_dim)
        """
        if self.pre_load_img:
            img = self.resnet(img)

        if self.scratch_embeddings and not self.fasttext_embeddings:
            h1 = self.relu(self.im_linear1(img))
            out = self.im_linear2(h1)
            return self.batch_norm(out)   # self.batch_norm(out) ==> batch_norm gave the best results
        elif not self.scratch_embeddings and not self.fasttext_embeddings:
            h1 = self.relu(self.im_linear1(img))
            out = self.im_linear2(h1)
            return self.batch_norm(out)  # self.batch_norm(out) ==> batch_norm gave the best results
        else:
            out = self.vision(img)
            return self.batch_norm(out)

    def forward(self, img, text, char_text=None):

        query_rep = self.query_embedding(text, char_text)
        im_rep = self.image_embedding(img)

        return query_rep, im_rep

    def compute_loss(self, query_rep, im_rep):
        """
        Compute loss from query representation and image representation using pairwise cosine similarity
        :param img: raw img (batch, n_channels, width, height)
        :param text: Tensor of size (batch, max_length), text must be token ids
        :return: loss value
        """

        cos_sim = cosine_similarity(query_rep, im_rep, pairwise=True)
        loss = -torch.log_softmax(cos_sim / self.temperature, dim=-1).diag()
        return loss.mean()

    def init_image_features(self):

        # Freeze weights
        for param in self.resnet.parameters():
            param.requires_grad = False
        # Remove classifier
        self.resnet.fc = nn.Identity()

    def init_layers(self, embedding_dim):
        if self.scratch_embeddings and not self.fasttext_embeddings:
            self.im_linear1 = nn.Linear(2048, 1024)
            self.im_linear2 = nn.Linear(1024, embedding_dim)
            nn.init.xavier_uniform_(self.im_linear1.weight)
            nn.init.xavier_uniform_(self.im_linear2.weight)
        else:
            self.im_linear1 = nn.Linear(2048, 1024)  # 768 to match first experiments
            self.im_linear2 = nn.Linear(1024, embedding_dim)  # To remove to load first experiments
            nn.init.xavier_uniform_(self.im_linear1.weight)
            nn.init.xavier_uniform_(self.im_linear2.weight)  # To remove to load first experiments

        if self.fasttext_embeddings:
            self.vision = nn.Sequential(self.im_linear1,
                                        self.relu,
                                        self.im_linear2)


    def init_fasttext_embedding_layer(self, fasttext_model, index_vocab):
        logger.info("Start loading pre-trained weights")
        pretrained_embedding = fasttext_model.get_input_matrix()[index_vocab]
        pretrained_embedding = np.concatenate((np.zeros((1, pretrained_embedding.shape[1])), pretrained_embedding))

        self.embedding = nn.Embedding.from_pretrained(embeddings=torch.from_numpy(pretrained_embedding),
                                                      freeze=False,
                                                      sparse=True,
                                                      padding_idx=0).float()

        # self.embedding.weight.data.copy_(torch.from_numpy(pretrained_embedding))

        logger.info("Finished : Pre-trained weights successfully loaded")

        pass



