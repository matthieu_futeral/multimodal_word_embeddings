
# -*- coding: utf-8 -*-

"""
Created on 05/05/2021

@author: matthieufuteral-peter
"""


# Import system package
import os
import pdb
import sys
import argparse

# Torch package
import torch
import torch.nn as nn
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel
from torch.utils.data.distributed import DistributedSampler
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import torch.multiprocessing as mp
from torch.optim.lr_scheduler import ReduceLROnPlateau

# Logger package
from logger import logger
from tqdm.auto import tqdm

# Util
import random
import time
import json
from uuid import uuid4
from util import early_stopping, MyDataset, multimodal_optimizers

# Computation
import numpy as np

# Model
from models import ImageVec


class Loss:
    def __init__(self):
        self.batch_loss = []
        self.epoch_loss = []

    def init_batch(self):
        self.batch_loss = []

    def update_batch(self, loss):
        self.batch_loss.append(loss)

    def update_epoch(self):
        self.epoch_loss.append(np.mean(self.batch_loss))
        self.init_batch()



class Trainer:
    def __init__(self, args, model, train_loader, val_loader, writer, optimizer, scheduler):

        self.args = args
        self.model = model
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.writer = writer
        self.train_loss = Loss()
        self.val_loss = Loss()

    def train(self):

        self.epoch_time = []

        for epoch in range(1, self.args.n_epochs + 1):

            # dist.barrier()
            if self.args.local_rank <= 0:
                logger.info("Epoch {} - Start TRAINING".format(epoch))

            t0 = time.time()  # start time

            # Training step
            self.run_epoch(epoch)
            if self.scheduler is not None:
                self.scheduler.step(self.train_loss.epoch_loss[-1])

            # Validation step
            #if not self.args.scratch_embeddings and not self.args.fasttext_embeddings:
            #    logger.info("Start evaluating on Validation set...")
            #    self.eval(epoch)

            time_elapsed = time.time() - t0
            logger.info("Epoch {} - Time elapsed : {}".format(epoch, time_elapsed))
            self.epoch_time.append(time_elapsed)

            #if epoch == 1:
            #    torch.save(self.model.state_dict(), os.path.join(self.args.path_save, f"model_{epoch}.pt"))
            #if not epoch % 10:
            #    torch.save(self.model.state_dict(), os.path.join(self.args.path_save, f"model_{epoch}.pt"))

            # Early stopping
            #if not self.args.scratch_embeddings and not self.args.fasttext_embeddings:
            flag = early_stopping(self.model, self.train_loss.epoch_loss, max_count=self.args.patience,   # self.val_loss.epoch_loss
                                  path_save=self.args.path_save, epoch=epoch)
            if flag:
                print("Stop training... Patience reached")
                break

        logger.info("Average time per epoch : {}".format(np.mean(self.epoch_time)))
        self.writer.close()

    def run_epoch(self, epoch):

        if self.args.device == "cuda":
            torch.cuda.synchronize()

        self.model.train()
        self.optimizer.zero_grad()

        for batch_idx, sample in tqdm(enumerate(self.train_loader)):

            if not self.args.character_level:

                img, tokens = sample
                img = img.to(self.args.device)
                tokens = tokens.to(self.args.device)

                query_rep, im_rep = self.model(img, tokens)

            else:

                img, tokens, char_tokens = sample
                img = img.to(self.args.device)
                tokens = tokens.to(self.args.device)
                char_tokens = char_tokens.to(self.args.device)

                query_rep, im_rep = self.model(img, tokens, char_tokens)

            loss = self.model.compute_loss(query_rep, im_rep)
            loss.backward()

            self.train_loss.update_batch(loss.data.item())

            if not (batch_idx + 1) % self.args.log_interval:

                n_iter = batch_idx + (epoch - 1) * len(self.train_loader)

                logger.info('Batch idx: {} \tLoss: {:.6f}'.format(batch_idx + 1,
                                                                  np.mean(self.train_loss.batch_loss)))

                self.writer.add_scalar('Loss/train', np.mean(self.train_loss.batch_loss), n_iter)
                # Summary for tensorboard
                #for name, weight in self.model.named_parameters():
                #    if 'resnet' not in name:
                #        self.writer.add_histogram(name, weight, n_iter)
                #        self.writer.add_histogram(f'{name}.grad', weight.grad, n_iter)

            if not (batch_idx + 1) % self.args.gradient_step:
                self.optimizer.step()
                self.optimizer.zero_grad()

        del img, tokens  # free memory
        self.train_loss.update_epoch()

        logger.info("Train loss over Epoch {} : {:.6f}".format(epoch, self.train_loss.epoch_loss[-1]))

    def eval(self, epoch):

        self.model.eval()

        for batch_idx, sample in tqdm(enumerate(self.val_loader)):

            if not self.args.character_level:

                img, tokens = sample
                img = img.to(self.args.device)
                tokens = tokens.to(self.args.device)

                with torch.no_grad():
                    query_rep, im_rep = self.model(img, tokens)

            else:

                img, tokens, char_tokens = sample
                img = img.to(self.args.device)
                tokens = tokens.to(self.args.device)
                char_tokens = char_tokens.to(self.args.device)

                with torch.no_grad():
                    query_rep, im_rep = self.model(img, tokens, char_tokens)

            with torch.no_grad():
                loss = self.model.compute_loss(query_rep, im_rep)

            self.val_loss.update_batch(loss.data.item())

            if not batch_idx % self.args.log_interval:
                n_iter = batch_idx + (epoch - 1) * len(self.val_loader)

                logger.info('Batch idx: {} \tLoss: {:.6f}'.format(batch_idx, np.mean(self.val_loss.batch_loss)))
                self.writer.add_scalar('Loss/validation', np.mean(self.val_loss.batch_loss), n_iter)

            del img, tokens  # Free memory

        self.val_loss.update_epoch()
        logger.info("Validation loss over Epoch {} : {:.6f}".format(epoch, self.val_loss.epoch_loss[-1]))





class func_args:
    def __init__(self, kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)




def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--is_large", action="store_true", help="using large dataset")
    parser.add_argument("--n_epochs", type=int, default=100, help="number of epochs")
    parser.add_argument("--batch_size", type=int, default=512, help="size of the batch")
    parser.add_argument("--lr", type=float, default=1e-4, help="learning rate")
    parser.add_argument("--weight_decay", type=float, default=0.0, help="weigth decay")
    parser.add_argument("--debug", action="store_true", help="Use python debogger or not")
    parser.add_argument("--patience", type=int, default=7, help="How many epochs to wait before early stopping")
    parser.add_argument("--gradient_step", type=int, default=1, help="Number of gradient update wait, > 1 to be active")
    parser.add_argument("--log_interval", type=int, default=100, help="log interval")
    parser.add_argument("--language", type=str, nargs="+", default="", help="Language to train the model on")
    parser.add_argument("--exp_name", type=str, required=True, help="Name of the experiment")
    parser.add_argument("--embedding_dim", type=int, default=768)
    parser.add_argument("--hidden_dim", type=int, default=768)
    parser.add_argument("--num_workers", type=int, default=16)
    parser.add_argument("--pre_load_img", action="store_true", help="Load images or resnet50 features")
    parser.add_argument("--scratch_embeddings", action="store_true", help="Scratch word embeddings")
    parser.add_argument("--temperature", type=float, default=1.)
    parser.add_argument("--character_level", action="store_true", help="Character level embedding")
    parser.add_argument("--fasttext_embeddings", action="store_true", help="Use FastText Embeddings")
    parser.add_argument("--local_rank", type=int, default=-1, help="-1 to disable")
    args = parser.parse_args()

    if not args.debug:
        pdb.set_trace = lambda: None

    # Generate an id
    RUN_ID = str(uuid4())[0:5]

    if args.local_rank == -1:
        args.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        args.n_gpu = torch.cuda.device_count()
    else:
        args.device = torch.cuda.device(args.local_rank)
        torch.distributed.init_process_group(backend='nccl', init_method='env://')
        torch.cuda.set_device(args.local_rank)
        args.step_n_gpus = torch.distributed.get_world_size()

    # Set device
    """
    args.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device_ids = list(range(torch.cuda.device_count()))
    gpus = len(device_ids)
    """

    # Set seeds
    torch.cuda.manual_seed_all(42)
    torch.manual_seed(42)
    np.random.seed(42)
    random.seed(42)

    if args.local_rank <= 0:
        logger.info(f"RUN ID : {RUN_ID} - currently running on device : {'cuda' if torch.cuda.is_available() else 'cpu'}")
        logger.info(f"Is LARGE ? {args.is_large}")
        logger.info(f"Training Word Embeddings from scratch ? {args.scratch_embeddings}")
        logger.info(f"Character Level ? {args.character_level}")
        print(vars(args))

    # ------------------------------------ Prepare Datasets ------------------------------------------------------------
    """
    if not args.scratch_embeddings and not args.fasttext_embeddings:
        train_set = MyDataset(large=args.is_large, data_augmentation=True, mode='train', lang=args.language,
                              pre_load_img=args.pre_load_img, character_level=args.character_level)
        val_set = MyDataset(large=args.is_large, data_augmentation=True, mode='dev', lang=args.language,
                            pre_load_img=args.pre_load_img, character_level=args.character_level)

        if args.local_rank != -1:
            train_sampler = DistributedSampler(train_set, shuffle=True)
            val_sampler = DistributedSampler(val_set, shuffle=True)
            _shuffle = False
        else:
            train_sampler = None
            val_sampler = None
            _shuffle = True

        # Dataloader
        train_loader = DataLoader(train_set,
                                  batch_size=args.batch_size,
                                  shuffle=_shuffle,
                                  sampler=train_sampler,
                                  collate_fn=train_set.collate_fn,
                                  num_workers=args.num_workers)

        val_loader = DataLoader(val_set,
                                batch_size=args.batch_size,
                                shuffle=_shuffle,
                                sampler=val_sampler,
                                collate_fn=val_set.collate_fn,
                                num_workers=args.num_workers)

        vocab_len = len(train_set.tokenizer)
        char_vocab_len = None
    """

    # else:

    dataset = MyDataset(large=args.is_large, data_augmentation=True, mode="train", lang=args.language,
                        pre_load_img=args.pre_load_img, scratch_embeddings=args.scratch_embeddings,
                        character_level=args.character_level, fasttext_embeddings=args.fasttext_embeddings)

    if args.local_rank != -1:
        sampler = DistributedSampler(dataset, shuffle=True)
        _shuffle = False
    else:
        _shuffle = True
        sampler = None

    dataloader = DataLoader(dataset, batch_size=args.batch_size, shuffle=_shuffle, sampler=sampler,
                            collate_fn=dataset.collate_fn, num_workers=args.num_workers)
    vocab_len = dataset.len_vocab
    char_vocab_len = dataset.len_char_vocab if args.character_level else None

    if args.local_rank <= 0:
        logger.info("LEN VOCAB : {}".format(vocab_len))
        if args.character_level:
            logger.info("LEN CHARACTER VOCAB : {}".format(char_vocab_len))

    # ------------------------------------ Prepare Model and tokenizer -------------------------------------------------
    model = ImageVec(vocab_size=vocab_len,
                     embedding_dim=args.embedding_dim,
                     hidden_dim=args.hidden_dim,
                     scratch_embeddings=args.scratch_embeddings,
                     pre_load_img=args.pre_load_img,
                     temperature=args.temperature,
                     character_level=args.character_level,
                     fasttext_embeddings=args.fasttext_embeddings,
                     fasttext_model=None if not args.fasttext_embeddings else dataset.ft,
                     char_vocab_size=char_vocab_len,
                     index_vocab=None if not args.fasttext_embeddings else dataset.index_vocab_init_model)
    model.to(args.device)

    #if torch.cuda.device_count() > 1:
    #    model = nn.DataParallel(model)     # Only works on a single machine with multiple GPUs
    
    if args.local_rank != -1:
        model = DistributedDataParallel(model, device_ids=[args.local_rank], output_device=args.local_rank,
                                        find_unused_parameters=True)

    if not args.fasttext_embeddings:
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
        scheduler = ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=3, threshold=0.001,
                                      threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=True)
    else:
        optimizer_embedding = optim.SparseAdam(model.embedding.parameters(), lr=args.lr)
        optimizer_vision = optim.Adam(model.vision.parameters(), lr=args.lr)
        optimizer = multimodal_optimizers(optimizer_embedding, optimizer_vision)
        scheduler = None

    # Prepare Saving results
    if torch.cuda.is_available():
        folder_id = os.path.join("results", RUN_ID + "_" + os.environ.get("SLURM_JOB_ID") + "-" + args.exp_name)
    else:
        folder_id = os.path.join("results", RUN_ID + "_" + args.exp_name)
    os.makedirs(folder_id, exist_ok=False)
    results = vars(args)

    with open(os.path.join(folder_id, "vocab.json"), "w") as fj:
        #if not args.scratch_embeddings and not args.fasttext_embeddings:
        #    json.dump(train_set.vocabulary, fj)
        #else:
        json.dump(dataset.vocabulary, fj)

    if args.character_level:
        with open(os.path.join(folder_id, "char_vocab.json"), "w") as fj:
            json.dump(dataset.char_vocabulary, fj)

    if args.fasttext_embeddings:
        with open(os.path.join(folder_id, "ft_vocabulary_to_vocab_idx.json"), "w") as fj:
            json.dump(dataset.ft_vocabulary_to_vocab_idx, fj)

    args.path_save = folder_id
    writer = SummaryWriter(log_dir=args.path_save + '/runs')

    # ------------------------------------ Training --------------------------------------------------------------------

    #if not args.scratch_embeddings and not args.fasttext_embeddings:
    #    trainer = Trainer(args, model, train_loader, val_loader, writer, optimizer, scheduler)
    #else:
    trainer = Trainer(args, model, dataloader, dataloader, writer, optimizer, scheduler)

    trainer.train()

    results['train_loss'] = trainer.train_loss.epoch_loss
    results['val_loss'] = trainer.val_loss.epoch_loss
    results['epoch_time'] = trainer.epoch_time

    with open(os.path.join(folder_id, "results.json"), 'w') as fj:
        json.dump(results, fj)


if __name__ == "__main__":
    main()


