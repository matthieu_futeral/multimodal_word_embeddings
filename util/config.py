# -*- coding: utf-8 -*-

"""
Created on 10/06/2021

@author: matthieufuteral-peter
"""



def parse_config(path_cfg):
    with open(path_cfg, 'r') as f:
        lines = f.read().split('\n')
    lines = [line.strip() for line in lines if line and not line.startswith('#')]

    modules = []
    for l in lines:
        if l.startswith('['):
            modules.append({})
            new_type = l[1:-1].strip()
            modules[-1]['type'] = new_type
        else:
            k, v = l.split('=')
            modules[-1][k.strip()] = v.strip()
    return modules


