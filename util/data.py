# -*- coding: utf-8 -*-

"""
Created on 05/05/2021

@author: matthieufuteral-peter
"""

import os
import time
import random
import json
import gzip
import pdb
import re
from tqdm import tqdm
import glob
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader, IterableDataset
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data._utils.collate import default_collate
from logger import logger
import fasttext

# Vision
from PIL import Image
import accimage
import torchvision
import torchvision.transforms as T

# NLP
from transformers import BertTokenizer



def to_tensor(pic):
    nppic = np.zeros([pic.channels, pic.height, pic.width], dtype=np.float32)
    pic.copyto(nppic)
    return torch.from_numpy(nppic)


class MyDataset(Dataset):

    def __init__(self, large: bool, data_augmentation: bool, mode: str, lang: list, pre_load_img=False,
                 scratch_embeddings=False, character_level=False, fasttext_embeddings=False):
        super(MyDataset, self).__init__()

        # Params
        self.scratch_embeddings = scratch_embeddings
        self.fasttext_embeddings = fasttext_embeddings
        self.augment = data_augmentation
        self.is_large = large
        self.mode = mode
        self.lang = lang
        self.pre_load_img = pre_load_img
        self.character_level = character_level
        assert mode in ['train', 'dev', 'test'], "mode should be either 'train', 'dev', or 'test'"

        if self.is_large:
            self.path_data = os.path.join("data", "mmid_large")
        else:
            self.path_data = os.path.join("data", "mmid_mini")

        with open(self.path_data + '/dict.json', 'r') as fj:
            self.id2word = json.load(fj)

        if not self.scratch_embeddings and not self.fasttext_embeddings:

            self.path_train = os.path.join(self.path_data, "train.txt")
            self.path_test = os.path.join(self.path_data, "test.txt")
            self.path_dev = os.path.join(self.path_data, "dev.txt")
            with open(self.path_train, 'r') as f:
                self.data = f.read().split('\n')[:-1]
            with open(self.path_test, 'r') as f:
                self.data += f.read().split('\n')[:-1]
            with open(self.path_dev, 'r') as f:
                self.data += f.read().split('\n')[:-1]

            #self.path_dataset = os.path.join(self.path_data, f"{self.mode}.txt")
            #with open(self.path_dataset, 'r') as f:
            #    self.data = f.read().split('\n')[:-1]

            self.build_vocab()
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')

        elif not self.scratch_embeddings and self.fasttext_embeddings:
            self.path_train = os.path.join(self.path_data, "train.txt")
            self.path_test = os.path.join(self.path_data, "test.txt")
            self.path_dev = os.path.join(self.path_data, "dev.txt")
            with open(self.path_train, 'r') as f:
                self.data = f.read().split('\n')[:-1]
            with open(self.path_test, 'r') as f:
                self.data += f.read().split('\n')[:-1]
            with open(self.path_dev, 'r') as f:
                self.data += f.read().split('\n')[:-1]

            self.build_vocab()
            self.init_fasttext_processing()
            self.max_length_char = 128
            self.max_n_word_query = 20


        else:
            self.path_train = os.path.join(self.path_data, "train.txt")
            self.path_test = os.path.join(self.path_data, "test.txt")
            self.path_dev = os.path.join(self.path_data, "dev.txt")
            with open(self.path_train, 'r') as f:
                self.data = f.read().split('\n')[:-1]
            with open(self.path_test, 'r') as f:
                self.data += f.read().split('\n')[:-1]
            with open(self.path_dev, 'r') as f:
                self.data += f.read().split('\n')[:-1]

            # Build vocabulary
            self.build_vocab()

        if self.lang:
            lang_data = []
            for path in tqdm(self.data):
                for lg in self.lang:
                    if lg in path:
                        lang_data.append(path)
                        break
            self.data = lang_data

        self.lang_path = [lang for lang in glob.glob(self.path_data + '/*') if ".txt" not in lang]

    def build_vocab(self):
        lang_keys = self.lang
        if "english" in self.lang:
            lang_keys += [f"english-{str(i).zfill(2)}" for i in range(1, 28)]

        self.id2word = {k: v for k, v in self.id2word.items() if k in lang_keys}
        self.word2id = {lang: {v: k for k, v in self.id2word[lang].items()} for lang in self.id2word.keys()}

        queries = []
        for vocab in self.word2id.values():
            queries += list(vocab.keys())

        words = []
        for query in queries:
            if not self.fasttext_embeddings:
                clean_query = re.sub("\W+", ' ', query.lower()).strip()
                words += [w.lower().strip() for w in clean_query.split()]
            else:
                words += [w.strip() for w in query.split()]

        words = [w for w in list(set(words)) if w]
        self.vocabulary = {w: idx for idx, w in enumerate(words, 1)}
        self.len_vocab = len(self.vocabulary) + 1

        if self.character_level:
            characters = []
            for query in queries:
                characters += list(query)

            characters = list(set(characters))
            self.char_vocabulary = {c: idx for idx, c in enumerate(characters, 1)}
            self.len_char_vocab = len(self.char_vocabulary) + 1

        del self.word2id  # free memory

    def __getitem__(self, item):

        try:
            if not self.character_level:
                img, word = self.load_txt_img(item)
            else:
                img, word, char_word = self.load_txt_img(item)
            # Language processing
            if not self.scratch_embeddings and not self.fasttext_embeddings:
                tokens = self.tokenizer(word, add_special_tokens=False, padding='max_length', return_tensors='pt')['input_ids']
                tokens = tokens.squeeze()
            elif not self.scratch_embeddings and self.fasttext_embeddings:
                tokens = torch.LongTensor(self.format_subword_tokens(word))
            else:
                tokens = torch.LongTensor([self.vocabulary[w] for w in word])

            if word:
                if not self.character_level:
                    return img, tokens
                else:
                    char_tokens = torch.LongTensor([self.char_vocabulary[c] for c in char_word])
                    return img, tokens, char_tokens
            else:
                return None

        except Exception as e:
            print(e)
            return None

    def __len__(self):
        return len(self.data)

    def collate_fn(self, batch):
        r"""Puts each data field into a tensor with outer dimension batch size"""
        if not self.fasttext_embeddings:
            batch = [d for d in batch if d is not None]
            if not self.character_level:
                imgs, texts = list(zip(*batch))
            else:
                imgs, texts, char_texts = list(zip(*batch))
                char_texts = pad_sequence(char_texts, batch_first=True, padding_value=0)

            texts = pad_sequence(texts, batch_first=True, padding_value=0)

            if not self.character_level:
                return torch.stack(imgs), texts
            else:
                return torch.stack(imgs), texts, char_texts
        else:
            batch = [d for d in batch if d is not None]
            imgs, texts = list(zip(*batch))
            return torch.stack(imgs), torch.stack(texts)

    @staticmethod
    def data_transform(size=256, augment=False, accimage=False):
        output = [T.Resize((size, size))]
        if augment:
            output.append(T.RandomHorizontalFlip(.5))
        if accimage:
            output.append(to_tensor)
        else:
            output.append(T.ToTensor())
        output += [T.Normalize(mean=[0.485, 0.456, 0.406],
                               std=[0.229, 0.224, 0.225])]
        return T.Compose(output)

    def load_txt_img(self, item):

        path = os.path.join(self.path_data, self.data[item][2:])

        # Load data
        lang, idx = path.split('/')[-3], path.split('/')[-1]

        if self.is_large:
            if self.pre_load_img:
                img_id = random.choice([im_id for im_id in os.listdir(path) if '.jpg' in im_id])
            else:
                img_id = random.choice([im_id for im_id in os.listdir(path) if 'features.npy' in im_id])
        else:
            if self.pre_load_img:
                img_id = '01.jpg'
            else:
                img_id = '01_resnet50_features.npy'
        im_path = os.path.join(path, img_id)

        # Text processing
        if not self.scratch_embeddings and not self.fasttext_embeddings:
            words = self.id2word[lang][idx]
        elif not self.scratch_embeddings and self.fasttext_embeddings:
            words = self.id2word[lang][idx].strip().split()
        else:
            query = self.id2word[lang][idx]
            clean_query = re.sub("\W+", ' ', query.lower()).strip()
            words = [w.lower().strip() for w in clean_query.split() if w]
            if self.character_level:
                char_words = list(query)

        # Image processing
        if self.pre_load_img:
            try:
                img = accimage.Image(im_path)
                image_processor = self.data_transform(augment=self.augment,
                                                      accimage=True)
            except:
                img = Image.open(im_path).convert("RGB")
                image_processor = self.data_transform(augment=self.augment)

            img = image_processor(img)
        else:
            img = torch.from_numpy(np.load(im_path))

        if not self.character_level:
            return img, words
        else:
            return img, words, char_words

    def init_fasttext_processing(self):
        logger.info("Start init FastText Load vocab")
        path_fasttext = "./data/fasttext/wit_trained"
        path_model = os.path.join(path_fasttext, "model.bin")
        self.ft = fasttext.load_model(path_model)

        self.ft_vocabulary = {w: self.ft.get_word_id(w) + 1 for w in self.vocabulary.keys() if self.ft.get_word_id(w) != -1}
        self.ft_vocabulary_to_vocab_idx = {self.ft.get_word_id(w) + 1: self.vocabulary[w] for w in self.vocabulary.keys() if
                                           self.ft.get_word_id(w) != -1}
        self.ft_vocabulary_to_vocab_idx = dict(sorted(self.ft_vocabulary_to_vocab_idx.items(), key=lambda item: item[1]))
        self.len_ft_vocab = len(self.ft_vocabulary)
        self.index_vocab_init_model = list(self.ft_vocabulary_to_vocab_idx.keys())

        self.start_subword_idx = self.ft.get_output_matrix().shape[0]
        self.end_subword_idx = self.ft.get_input_matrix().shape[0]
        self.shift_subword_idx = self.start_subword_idx - self.len_ft_vocab

        self.index_vocab_init_model += [i for i in range(self.start_subword_idx, self.end_subword_idx)]
        # self.len_vocab = self.ft.get_input_matrix().shape[0] + 1
        logger.info("Finish init FastText Vocabulary")

    def format_subword_tokens(self, words):

        tokens = []
        words = words[:self.max_n_word_query]
        for w in words:
            final_tokens = []
            sub_tokens = (self.ft.get_subwords(w)[1] + 1).tolist()

            if 1 <= sub_tokens[0] <= self.start_subword_idx:
                final_tokens.append(self.ft_vocabulary_to_vocab_idx[sub_tokens[0]])
            else:
                final_tokens.append(self.convert_ft_subword_idx_to_vocab_idx(sub_tokens[0]))

            for sub_tok in sub_tokens[1:]:
                final_tokens.append(self.convert_ft_subword_idx_to_vocab_idx(sub_tok))

            if len(final_tokens) >= self.max_length_char:
                final_tokens = final_tokens[:self.max_length_char]
            else:
                final_tokens += [0] * (self.max_length_char - len(final_tokens))

            tokens.append(final_tokens)

        if len(words) < self.max_n_word_query:
            tokens += [[0] * self.max_length_char for _ in range(self.max_n_word_query - len(words))]

        return tokens

    def convert_ft_subword_idx_to_vocab_idx(self, subword_id):
        return subword_id - self.shift_subword_idx



"""
class wacky_Dataset(IterableDataset):

    def __init__(self, data_augmentation: bool, neg_k: int):
        super(wacky_Dataset, self).__init__()

        # Params
        self.augment = data_augmentation
        self.neg_k = neg_k

        self.path_data = os.path.join("data", "mmid_large")

        with open(self.path_data + '/dict.json', 'r') as fj:
            self.dic_mmid = json.load(fj)

        self.process_dict_mmid()
        del self.dic_mmid

        # NLP
        self.path_wiki_data = os.path.join("data", "wackypedia", "text_data", 'wackypedia.txt')

        # Vocabulary
        self.path_vocab = os.path.join("data", "wackypedia", "text_data", "vocab.json")
        with open(self.path_vocab, 'r') as fj:
            self.word2id_wacky = json.load(fj)

    def process_dict_mmid(self):
        self.word2id_mmid = {}
        for k, v in self.dic_mmid.items():
            if 'english' in k:
                self.word2id_mmid.update({val: [k, key] for key, val in v.items()})

    def pipeline_preprocess(self, text):

        t0 = time.time()
        text = text.split('\t')[-1]
        text = text.strip().split(' ')
        idx = np.random.randint(len(text))
        target_w = re.sub('\t', '', text[idx])
        context_w = text[max(idx - self.neg_k, 0): idx] + text[(idx+1): min(idx + self.neg_k, len(text))]
        context_w = [re.sub("\t", '', txt) for txt in context_w]

        target_pp = torch.Tensor([self.word2id_wacky[target_w]])
        context_pp = torch.Tensor([self.word2id_wacky[tok] for tok in context_w])
        print(f"Time to handle NLP preprocessing : {time.time() - t0}")

        t0 = time.time()
        img = self.load_img(target_w)
        print(f"Time to load positive sample img : {time.time() - t0}")
        
        if self.neg_k:
            t0 = time.time()
            neg_text = list(np.random.choice(list(self.word2id_wacky.values()), self.neg_k))
            print(f"Time to load negative samples text : {time.time() - t0}")
            t0 = time.time()
            neg_img = self.load_neg_img()
            print(f"Time to load negative samples img : {time.time() - t0}")

            return target_pp, context_pp, neg_text, img, neg_img
        else:
            
        return target_pp, context_pp, img

    def line_mapper(self, line):

        text = line
        return self.pipeline_preprocess(text)

    def load_img(self, text):

        if text not in self.word2id_mmid.keys():
            return None
        else:
            lang, idx = self.word2id_mmid[text]
            path = os.path.join(self.path_data, lang, f"scale-{lang}-package", idx)
            img_id = random.choice([im_id for im_id in os.listdir(path) if '.jpg' in im_id])
            im_path = os.path.join(path, img_id)
            # Image processing
            try:
                img = accimage.Image(im_path)
                image_processor = self.data_transform(augment=self.augment,
                                                      accimage=True)
            except:
                img = Image.open(im_path).convert("RGB")
                image_processor = self.data_transform(augment=self.augment)

            img = image_processor(img)
            return img

    def load_neg_img(self):

        dataset = np.random.choice(["train", "dev", "test"], p=[.8, .1, .1])
        neg_samples_folder = list(np.random.choice(open(self.path_data + f'/{dataset}.txt', 'r').read().split("\n")[:-1],
                                                   self.neg_k))
        neg_img_samples = []
        for im_folder in neg_samples_folder:
            im_global_folder_path = self.path_data + im_folder[1:]
            im_path = np.random.choice(glob.glob(im_global_folder_path + '/*jpg'))
            try:
                img = accimage.Image(im_path)
                image_processor = self.data_transform(augment=self.augment,
                                                      accimage=True)
            except:
                img = Image.open(im_path).convert("RGB")
                image_processor = self.data_transform(augment=self.augment)
            img = image_processor(img)
            neg_img_samples.append(img)

        return torch.stack(neg_img_samples)

    @staticmethod
    def data_transform(size=256, augment=False, accimage=False):
        output = [T.Resize((size, size))]
        if augment:
            output.append(T.RandomHorizontalFlip(.5))
        if accimage:
            output.append(to_tensor)
        else:
            output.append(T.ToTensor())
        output += [T.Normalize(mean=[0.485, 0.456, 0.406],
                               std=[0.229, 0.224, 0.225])]
        return T.Compose(output)

    def __iter__(self):

        # Create an iterator
        data_itr = open(self.path_wiki_data)

        # Map each element using the line_mapper
        mapped_itr = map(self.line_mapper, data_itr)

        return mapped_itr
"""


class wit_dataset(Dataset):
    def __init__(self):
        super(wit_dataset, self).__init__()

        with gzip.open("wit_v1.train.all-00002-of-00010.tsv.gz", "rb") as f:
            for line in f:
                print(line)
                time.sleep(4)

        f = gzip.open("wit_v1.train.all-00002-of-00010.tsv.gz", "rb")
        size_file = f.tell()
        n = random.randrange(size_file)
        f.seek(n)


if __name__ == "__main__":

    from multiprocessing import cpu_count
    print("CPU Count : {}".format(cpu_count()))

    train_set = MyDataset(large=True, data_augmentation=True, mode='train',
                          lang=["english", "spanish", "german", "italian"], pre_load_img=False, scratch_embeddings=False,
                          character_level=False, fasttext_embeddings=True)
    train_loader = DataLoader(train_set,
                              batch_size=16,
                              shuffle=True,
                              sampler=None,
                              collate_fn=train_set.collate_fn,
                              num_workers=0)

    pdb.set_trace()
    for img, tokens in tqdm(train_loader):
        pdb.set_trace()

