# -*- coding: utf-8 -*-

"""
Created on 05/05/2021

@author: matthieufuteral-peter
"""


import os
import pdb
from logger import logger
import torch
import numpy as np
import torch.distributed as dist


def cosine_similarity(A: torch.Tensor or np.ndarray,
                      B: torch.Tensor or np.ndarray,
                      batch=True,
                      pairwise=False):
    """
    Compute cosine similarity
    :param A: Tensor or ndarray of shape (n, )
    :param B: Tensor or ndarray of shape (n, ), same shape and type as A
    :param batch: Compute cosine similarity with first dim as batch (n_batch, n) (Default: True)
    :param pairwise: Compute cosine similarity between all samples in the same batch to perform softmax (Default: False)
    :return: cosine similarity A^T * B / || A || * || B ||
    """

    # assert A.shape == B.shape, "A and B must be same shape"

    if isinstance(A, torch.Tensor):
        assert type(A) == type(B), "B must be same type as A (torch.Tensor)"
        if batch:
            if pairwise:
                return (A @ B.T) / (B.norm(dim=1) * A.norm(dim=1).unsqueeze(1))
            else:
                return torch.bmm(A.unsqueeze(1), B.unsqueeze(-1)).squeeze() / (A.norm(dim=1) * B.norm(dim=1))
        else:
            return (A @ B) / (A.norm() * B.norm())

    elif isinstance(A, np.ndarray):
        assert type(A) == type(B), "B must be same type as A (np.ndarray)"
        if batch:
            return np.diag(np.tensordot(A, B, axes=(1, 1))) / (np.linalg.norm(A, axis=1) * np.linalg.norm(B, axis=1))
        else:
            return (A @ B) / (np.linalg.norm(A) * np.linalg.norm(B))

    else:
        raise TypeError("A and B must be either np.ndarray or torch.Tensor type")


def early_stopping(model, val_loss: list, max_count: int, path_save: str, epoch: int):
    global ref_value, count
    if len(val_loss) == 1:
        logger.info("Save model ... Epoch 1")
        ref_value = val_loss[-1]
        count = 0
        flag = False
        torch.save(model.state_dict(), os.path.join(path_save, "model.pt"))
    else:
        if val_loss[-1] < ref_value:
            ref_value = val_loss[-1]
            count = 0
            flag = False
            logger.info("Save model ... Epoch {}".format(epoch))
            torch.save(model.state_dict(), os.path.join(path_save, "model.pt"))
        else:
            count += 1
            if count == max_count:
                flag = True
            else:
                flag = False
    return flag


def load_model(model, path_model, device):
    state_dict = torch.load(path_model, map_location=device)
    model.load_state_dict(state_dict)
    logger.info("Weights loaded")
    return model


class multimodal_optimizers(object):
    def __init__(self, *op):
        self.optimizers = op

    def zero_grad(self):
        for op in self.optimizers:
            op.zero_grad()

    def step(self):
        for op in self.optimizers:
            op.step()


def distributed_setup(rank, num_devices):
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '8008'
    dist.init_process_group(backend='mpi', init_method='env://', world_size=num_devices, rank=rank)
    pass

def cleanup():
    dist.destroy_process_group()
    pass








